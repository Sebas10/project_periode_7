﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TroopHealthBar : MonoBehaviour
{
    [SerializeField]
    private int m_HealthBarHeight = 1;

    [SerializeField]
    private GameObject m_BarPreFab;
    public Image m_HealthBar;
    public Image m_HealthBarFilled;

    [SerializeField] private float m_Multiplier;
    [SerializeField] private float m_StartHealth;

    private void Start()
    {
        m_HealthBar = Instantiate(m_BarPreFab, FindObjectOfType<Canvas>().transform).GetComponent<Image>();
        m_HealthBarFilled = new List<Image>(m_HealthBar.GetComponentsInChildren<Image>()).Find(img => img != m_HealthBar);

        if(m_StartHealth > 0)
        {
            m_HealthBarFilled.fillAmount = m_StartHealth;
        }
    }

    private void Update()
    {
        m_HealthBar.transform.position = Camera.main.WorldToScreenPoint(transform.position + new Vector3(0, m_HealthBarHeight, 0));
        ZeroHealth();
    }

    public void Damage(float DamageAmount)
    {
        m_HealthBarFilled.fillAmount -= (DamageAmount / m_Multiplier);
    }

    public void Heal(float HealAmount)
    {
        m_HealthBarFilled.fillAmount += (HealAmount / m_Multiplier);
    }

    private void ZeroHealth()
    {
        if (m_HealthBarFilled.fillAmount <= 0)
        {
            m_HealthBarFilled.fillAmount = 0;
        }
    }

    private void MaxHealth()
    {
        if (m_HealthBarFilled.fillAmount >= 1)
        {
            m_HealthBarFilled.fillAmount = 1;
        }
    }
}
