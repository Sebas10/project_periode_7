﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthHandler : MonoBehaviour
{
    [SerializeField]
    private TroopHealthBar m_Bar;
    [SerializeField]
    private float HealthNummers;

    public void Update()
    {       
        if (m_Bar == null)
        {
            m_Bar = GameObject.FindObjectOfType<TroopHealthBar>();
        }
    }

    public void DoeDieDamage()
    {
        //testing damage
        m_Bar.Damage(HealthNummers);
    }

    public void DoeDieHeal()
    {
        //testing heal
        m_Bar.Heal(HealthNummers);
    }
}
