﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMap : MonoBehaviour
{
    [Header("Empty Gameobject Start Pos")]
    [SerializeField]
    private Transform m_Start;

    [Header("Start Kamer")]
    [SerializeField]
    private GameObject m_StartRoom;
    [Header("End Kamer")]
    [SerializeField]
    private GameObject m_EndRoom;
    [Header("Random Level Kamers")]
    [SerializeField]
    private GameObject[] m_Room;

    private int m_Direction = 1;
    [Header("Ruimte tussen de Kamers")]
    [SerializeField]
    private float m_RoomDistance;

    private float m_TimeBtwRoom;
    [Header("Tijd tussen het spawnen van Kamers")]
    [SerializeField]
    private float m_StartTimeBtwRoom = 0.25f;

    private int m_RandStartPos;

    private int m_MapSize;
    [Header("Min and Max Mapsize")]
    [SerializeField]
    private int m_MapSizeMin = 2;
    [SerializeField]
    private int m_MapSizeMax = 5;

    private bool m_StopGen;

    private int m_Optellen = 1;

    void Start()
    {
        m_RandStartPos = Random.Range(0, m_Room.Length);
        m_MapSize = Random.Range(m_MapSizeMin, m_MapSizeMax +1);
        Debug.Log(m_MapSize);
        transform.position = m_Start.position;
        Instantiate(m_StartRoom, transform.position, Quaternion.identity);
    }

    void Update()
    {
        if (m_TimeBtwRoom <= 0 && m_StopGen == false)
        {
            Move();
            m_RandStartPos = Random.Range(0, m_Room.Length);
            m_TimeBtwRoom = m_StartTimeBtwRoom;
        }
        else
        {
            m_TimeBtwRoom -= Time.deltaTime;
        }
    }

    private void Move()
    {
        if(m_Direction == 1)
        {
            if (m_Optellen <= m_MapSize)
            {
                //choose random rooms
                Vector2 _NewPos = new Vector2(transform.position.x + m_RoomDistance, transform.position.y);
                transform.position = _NewPos;

                Instantiate(m_Room[m_RandStartPos], transform.position, Quaternion.identity);
                m_Optellen += 1;
            }
            else
            {
                m_Direction = 2;
            }
        }
        if(m_Direction == 2)
        {
            //create end room
            Vector2 _NewPos = new Vector2(transform.position.x + m_RoomDistance, transform.position.y);
            transform.position = _NewPos;

            Instantiate(m_EndRoom, transform.position, Quaternion.identity);

            m_StopGen = true;
        }
    }
}
