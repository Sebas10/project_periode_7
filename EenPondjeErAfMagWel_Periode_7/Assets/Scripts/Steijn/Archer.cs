﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Archer : MonoBehaviour
{
    EnemySearchPattern m_ESP;

    [Header("Friendly")]
    [SerializeField]
    private LayerMask m_Friendly;
    [SerializeField]
    private float m_RayRangeFriendly;
    [SerializeField]
    [Header("AI")]
    private LayerMask m_AI;
    [SerializeField]
    private float m_RayRangeAI;

    private float m_SaveSpeed;

    private void Start()
    {
        m_ESP = GetComponent<EnemySearchPattern>();
        m_SaveSpeed = m_ESP.m_Speed;
    }

    void Update()
    {
        //----------------------------------------------------------------------------------------------------------------------------------------
        //Friendly Check
        //----------------------------------------------------------------------------------------------------------------------------------------

        RaycastHit2D _Hit = Physics2D.Raycast(transform.position, transform.TransformDirection(Vector3.right), m_RayRangeFriendly, m_Friendly);

        if (_Hit.collider != null && _Hit.collider.gameObject != this.gameObject)
        {
            m_ESP.m_Speed = 0;
        }
        else
        {
            m_ESP.m_Speed = m_SaveSpeed;
        }

        //----------------------------------------------------------------------------------------------------------------------------------------
        //Ai Check
        //----------------------------------------------------------------------------------------------------------------------------------------

        RaycastHit2D _Hits = Physics2D.Raycast(transform.position, transform.TransformDirection(Vector3.right), m_RayRangeAI, m_AI);

        if (_Hits.collider != null && _Hits.collider.gameObject != this.gameObject)
        {
            m_ESP.m_Speed = 0;
        }
        else
        {
            m_ESP.m_Speed = m_SaveSpeed;
        }
    }
}
