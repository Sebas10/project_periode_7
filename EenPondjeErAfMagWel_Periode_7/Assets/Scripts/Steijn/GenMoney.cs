﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenMoney : MonoBehaviour
{
    [SerializeField]
    private BuyTroops m_BuyT;

    [SerializeField]
    private int m_GenGoldAmount;

    [SerializeField]
    private Text m_Gold;
    [SerializeField]
    private Text m_WorkerAmount;
    //Keep Zero
    public int m_Balance;

    [SerializeField]
    private float m_SetAfttellen = 2;
    private float m_Aftellen;

    void Update()
    {
        #region MoneyGen
        m_Aftellen -= 1 * Time.deltaTime;

        if (m_Aftellen <= 0)
        {
            m_Balance += m_GenGoldAmount;
            m_Aftellen = m_SetAfttellen;
        }
        #endregion

        m_Gold.text = m_Balance.ToString() + " : Gold";
        m_WorkerAmount.text = m_BuyT.m_Workers.ToString() + "/6";
    }
}
