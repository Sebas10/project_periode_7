﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyTroops : MonoBehaviour
{
    [SerializeField]
    private GenMoney m_GenM;

    [Header("Only workers are limited")]
    [SerializeField]
    private int m_WorkerLimit = 6;
    //keep zero
    public int m_Workers;

    [Header("Worker Preset")]
    [SerializeField]
    private GameObject m_WorkerPreFab;
    [SerializeField]
    private int m_Cost = 0;

    [Header("Soldier Preset")]
    [SerializeField]
    private GameObject m_SoldierPreFab;
    [SerializeField]
    private int m_Cost1 = 0;

    [Header("Archer Preset")]
    [SerializeField]
    private GameObject m_ArcherPreFab;
    [SerializeField]
    private int m_Cost2 = 0;

    [Header("Tank Preset")]
    [SerializeField]
    private GameObject m_TankPreFab;
    [SerializeField]
    private int m_Cost3 = 0;

    private bool m_MagWorkersKopen = true;

    [Header("Spawn Position")]
    [SerializeField]
    private Transform m_SpawnPoint;

    void Update()
    {
        if (m_Workers >= m_WorkerLimit)
        {
            m_MagWorkersKopen = false;
        }
    }

    public void BuyWorkers()
    {
        if (m_GenM.m_Balance >= m_Cost && m_MagWorkersKopen == true)
        {
            m_GenM.m_Balance -= m_Cost;
            m_Workers += 1;

            Instantiate(m_WorkerPreFab, m_SpawnPoint.position, Quaternion.identity);
        }
    }

    public void BuySoldier()
    {
        if (m_GenM.m_Balance >= m_Cost1)
        {
            m_GenM.m_Balance -= m_Cost1;

            Instantiate(m_SoldierPreFab, m_SpawnPoint.position, Quaternion.identity);
        }
    }

    public void BuyArcher()
    {
        if (m_GenM.m_Balance >= m_Cost2)
        {
            m_GenM.m_Balance -= m_Cost2;

            Instantiate(m_ArcherPreFab, m_SpawnPoint.position, Quaternion.identity);
        }
    }

    public void BuyTank()
    {
        if (m_GenM.m_Balance >= m_Cost3)
        {
            m_GenM.m_Balance -= m_Cost3;

            Instantiate(m_TankPreFab, m_SpawnPoint.position, Quaternion.identity);
        }
    }
}
