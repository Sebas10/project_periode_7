﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Worker : MonoBehaviour
{
    [SerializeField] private GameObject m_GoudZakje;

    //Voeg geld toe aan het totaal
    [SerializeField]
    private GenMoney m_GenM;

    private float m_Speed = 2;
    private float m_SaveSpeed;

    [SerializeField]
    private int m_ReturnMoneyAmount;

    [SerializeField]
    private bool m_GettingGold = true;
    [SerializeField]
    private bool m_ReturningGold = false;

    [SerializeField]
    private LayerMask m_WorkerLayer;
    [SerializeField]
    private float m_RayRange;

    private void Start()
    {
        m_GenM = FindObjectOfType<GenMoney>();
        m_SaveSpeed = m_Speed;
        m_GoudZakje.SetActive(false);
    }

    void Update()
    {
        if (m_GettingGold == true)
        {
            transform.rotation = new Quaternion(transform.rotation.x, 0f, transform.rotation.z, transform.rotation.w);
            m_GoudZakje.SetActive(false);
            transform.position = new Vector2(transform.position.x + m_Speed * Time.deltaTime, transform.position.y);

            RaycastHit2D _Hit = Physics2D.Raycast(transform.position, transform.TransformDirection(Vector3.right), m_RayRange, m_WorkerLayer);

            if (_Hit.collider != null && _Hit.collider.gameObject != this.gameObject)
            {
                m_Speed = 0;
            }
            else
            {
                m_Speed = m_SaveSpeed;
            }
        }
        else if (m_ReturningGold == true)
        {
            transform.rotation = new Quaternion(transform.rotation.x, 180f, transform.rotation.z, transform.rotation.w);
            m_GoudZakje.SetActive(true);
            transform.position = new Vector2(transform.position.x - m_Speed * Time.deltaTime, transform.position.y);
        }
    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        //als de Worker met de Base box collide m_GettingGold = true | m_ReturningGold = false;
        if (coll.gameObject.tag == "Base" && m_ReturningGold == true)
        {
            m_GettingGold = true;
            m_ReturningGold = false;
            m_GenM.m_Balance += m_ReturnMoneyAmount;
        }

        //als de Worker met de goldmine box collide m_GettingGold = false | m_ReturningGold = true;
        if (coll.gameObject.tag == "Mine" && m_GettingGold == true)
        {
            m_GettingGold = false;
            m_ReturningGold = true;
        }
    }
}
