﻿using UnityEngine;
using Cinemachine;

public class MoveCamera : MonoBehaviour
{
    public bool m_AmIPaused = false;

    [SerializeField]
    private Transform m_Camere_Pos;
    [SerializeField]
    private float m_Speed;
    [SerializeField]
    private Camera m_Camera;

    private RaycastHit hit;
    private bool m_IsFollowing = false;

    void FixedUpdate()
    {
        if (m_AmIPaused)
            return;

        MovementCamera();

        Ray ray = m_Camera.ScreenPointToRay(Input.mousePosition);
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit) && hit.transform.tag != "Pointer" && hit.transform.gameObject != null)
                m_IsFollowing = true;
            else
                MovementCamera();
        }

        if (m_IsFollowing)
            FollowTarget(hit.transform);
    }

    private void MovementCamera()
    {
        if(m_Camere_Pos.position.x <= -54f)
        {
            m_Camere_Pos.position = new Vector3(-53.99f, m_Camere_Pos.position.y, m_Camere_Pos.position.z);
        }
        else
        {
            if (Input.GetKey(KeyCode.A))
            {
                m_IsFollowing = false;
                m_Camere_Pos.position = new Vector3(m_Camere_Pos.position.x - (m_Speed * Time.deltaTime), m_Camere_Pos.position.y, -10.47f);
            }

            if (Input.GetKey(KeyCode.D))
            {
                m_IsFollowing = false;
                m_Camere_Pos.position = new Vector3(m_Camere_Pos.position.x + (m_Speed * Time.deltaTime), m_Camere_Pos.position.y, -10.47f);
            }
        }
    }

    private void FollowTarget(Transform target)
    {
        if (target != null)
        {
            float interpolation = 2.0f * Time.deltaTime;
            m_Camere_Pos.position = new Vector3(Mathf.Lerp(m_Camere_Pos.position.x, target.position.x, interpolation), m_Camere_Pos.position.y, m_Camere_Pos.position.z);
        }
    }
}

