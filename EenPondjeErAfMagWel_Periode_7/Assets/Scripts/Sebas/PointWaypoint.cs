﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum StateFriendly
{
    down = 0,
    middle,
    up,
    freewalk
};

public class PointWaypoint : MonoBehaviour
{
    [SerializeField]
    private StateEnemy m_State;

    [SerializeField]
    private GameObject[] m_Waypoints;

    public GameObject m_TargetWaypoint;

    private int m_NumState = 0;

    public bool m_AmIPaused = false;

    private float m_speedy = 2;

    private void Start()
    {
        m_TargetWaypoint = m_Waypoints[0];
    }

    private void Update()
    {
        if (m_AmIPaused)
            return;

        if (m_NumState > 2)
        {
            m_State = 0;
            m_NumState = 0;
        }

        switch (m_State)
        {
            case StateEnemy.down:
                m_TargetWaypoint = m_Waypoints[0];
                RotateFinger(-35f);
                break;
            case StateEnemy.middle:
                RotateFinger(0f);
                m_TargetWaypoint = m_Waypoints[1];
                break;
            case StateEnemy.up:
                RotateFinger(35f);
                m_TargetWaypoint = m_Waypoints[2];
                break;
            case State.freewalk:
                m_TargetWaypoint.transform.position = new Vector3(transform.position.x + m_speedy * Time.deltaTime, transform.position.y, transform.position.z);
                break;
            default:
                m_State = StateEnemy.middle;
                break;
        }
    }

    private void RotateFinger(float degree)
    {
        transform.rotation = Quaternion.Euler(0, 0, degree);
    }

    private void OnMouseDown()
    {
        m_State += 1;
        m_NumState += 1;
    }
}

