﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseManager : MonoBehaviour
{
    [SerializeField]
    private GameObject[] m_Objects;//Put player and weapon objects in this aray


    public List<GameObject> m_FriendlyUnits;//Put all Friendly's in aray that have the AI component
    public List<GameObject> m_EnemyUnits;//Put all enemy's in aray that have the AI component


    [SerializeField]
    private GameObject m_PauseScreen, UI_In_Game;

    public bool m_PauseB = false;

    private void Awake()
    {
        UI_In_Game.SetActive(true);
        m_PauseScreen.SetActive(false);
    }

    public void AddFriendly(GameObject enemy)
    {
        m_FriendlyUnits.Add(enemy);
    }

    public void AddEnemy(GameObject enemy)
    {
        m_EnemyUnits.Add(enemy);
    }

    void Update()
    {
        //Pause game with button P/esc and reset it if it's pressed again
        if (Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape))
        {
            m_PauseB = !m_PauseB;
        }

        if (m_PauseB)
        {
            Pause();
        }
        else
        {
            Continue();
        }

    }


    /// <summary>
    /// Pause all GameObjects that need to pause if paused screen is on
    /// </summary>
    private void Pause()
    {
        //Sets Bitmap on
        m_PauseScreen.SetActive(true);

        UI_In_Game.SetActive(false);

        for (int i = 0; i < m_EnemyUnits.Count; i++)
        {
            if (m_EnemyUnits[i] != null)
            {
                m_EnemyUnits[i].GetComponent<MovementEnemy>().m_AmIPaused = true;
            }
        }

        for (int i = 0; i < m_FriendlyUnits.Count; i++)
        {
            if (m_FriendlyUnits[i] != null)
            {
                m_FriendlyUnits[i].GetComponent<EnemySearchPattern>().m_AmIPaused = true;
            }
        }
        if (m_Objects[0] != null)
        {            
            m_Objects[0].GetComponent<MoveCamera>().m_AmIPaused = true;
            m_Objects[1].GetComponent<PointWaypoint>().m_AmIPaused = true;
            m_Objects[2].GetComponent<SpawnSystem_Enemy>().m_AmIPaused = true;
        }

    }

    /// <summary>
    /// Sets all GameObjects on in play mode. Paused GameObjects sets false
    /// </summary>
    private void Continue()
    {
        m_PauseScreen.SetActive(false);

        UI_In_Game.SetActive(true);

        for (int i = 0; i < m_EnemyUnits.Count; i++)
        {
            if (m_EnemyUnits[i] != null)
            {
                m_EnemyUnits[i].GetComponent<MovementEnemy>().m_AmIPaused = false;
            }
        }

        for (int i = 0; i < m_FriendlyUnits.Count; i++)
        {
            if (m_FriendlyUnits[i] != null)
            {
                m_FriendlyUnits[i].GetComponent<EnemySearchPattern>().m_AmIPaused = false;
            }
        }
        if (m_Objects[0] != null)
        {
            m_Objects[0].GetComponent<MoveCamera>().m_AmIPaused = false;
            m_Objects[1].GetComponent<PointWaypoint>().m_AmIPaused = false;
            m_Objects[2].GetComponent<SpawnSystem_Enemy>().m_AmIPaused = false;
        }
    }

    public void ContinueButton()
    {
        m_PauseB = false;
    }
}
