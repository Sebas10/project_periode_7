﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySearchPattern : MonoBehaviour
{
    public bool m_AmIPaused = false;
    
    public GameObject m_WayPointPrefab;

    private List<Transform> m_WayPoints;

    private List<string> m_RandomWaypointName;

    private Transform m_TargetWaypoint;
    private int m_CurrentWaypoint;

    private float m_MaxDist = 0.2f;
    public float m_Speed;

    private PauseManager m_PauseManager;

    void Start()
    {
        m_PauseManager = GameObject.FindObjectOfType<PauseManager>();
        m_PauseManager.AddFriendly(this.gameObject);

        AddWayPointPrefab();
    }

    void Update()
    {
        if (m_AmIPaused)
            return;


        if (m_WayPointPrefab == null)
        {
            AddWayPointPrefab();
            return;
        }


        if (m_WayPoints.Count > m_CurrentWaypoint || m_WayPoints != null)
        {
            if (m_CurrentWaypoint == 0)
            {
                m_WayPointPrefab = GameObject.FindObjectOfType<PointWaypoint>().m_TargetWaypoint;
                m_WayPoints.Clear();
                foreach (Transform t in m_WayPointPrefab.transform)
                {
                    m_WayPoints.Add(t);
                }
            }

            MovementUnit();
        }
        else
        {
            m_CurrentWaypoint = m_WayPoints.Count;
        }
    }

    /// <summary>
    /// Go to next waypoint in array
    /// </summary>
    /// <returns></returns>
    private IEnumerator NextWaypoint()
    {
        if (m_CurrentWaypoint < m_WayPoints.Count)
        {
            m_CurrentWaypoint += 1;
            yield return new WaitForSeconds(0.25f);
        }
    }

    /// <summary>
    /// Check for waypoints and move to them
    /// </summary>
    private void MovementUnit()
    {
        if (m_CurrentWaypoint < m_WayPoints.Count)
        {
            m_TargetWaypoint = m_WayPoints[m_CurrentWaypoint];

            //As long as distance between waypoint and enemy is bigger then max dist move towards waypoint
            if (Vector3.Distance(this.transform.position, m_TargetWaypoint.position) >= m_MaxDist)
            {
                transform.position = Vector3.MoveTowards(this.transform.position, m_TargetWaypoint.position, m_Speed * Time.deltaTime);
            }
            else// Go to next waypoint
                StartCoroutine(NextWaypoint());
        }
        else
        {
            //Pulling hier gebruiken
            transform.position = new Vector3(transform.position.x + m_Speed * Time.deltaTime, transform.position.y, transform.position.z);
        }
    }

    /// <summary>
    /// Rotate Enemy to look at the target waypoint
    /// </summary>
    private void LookAtWaypoint()
    {
        Vector3 delta = m_TargetWaypoint.position - transform.position;

        float angle = Mathf.Atan2(delta.y, delta.x) * 180.0f / Mathf.PI;

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, angle - 90.0f), 0.1f);
    }

    public void AddWayPointPrefab()
    {

        m_WayPointPrefab = GameObject.FindObjectOfType<PointWaypoint>().m_TargetWaypoint;

        if (m_WayPointPrefab == null)
            return;



        m_WayPoints = new List<Transform>();
        foreach (Transform t in m_WayPointPrefab.transform)
        {
            m_WayPoints.Add(t);
        }
        m_CurrentWaypoint = 0;
        m_TargetWaypoint = m_WayPoints[m_CurrentWaypoint];

        if (m_WayPoints.Count == 0)
            Debug.LogError("Geen waypoints gevonden");
    }
}
