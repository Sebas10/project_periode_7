﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TypeEnemy
{
    Soldier = 0,
    Archer,
    Tank
}

public class EnemyDetection : MonoBehaviour
{
    private RaycastHit m_Hit;
    public TypeEnemy m_TypeEnemy;

    private EnemySearchPattern m_ESP;

    [Header("Friendly")]
    [SerializeField]
    private LayerMask m_Friendly;
    [SerializeField]
    private float m_RayRangeFriendly;
    [SerializeField]
    [Header("AI")]
    private LayerMask m_AI;
    [SerializeField]
    private float m_RayRangeAI;

    private float m_SaveSpeed;

    void Start()
    {
        m_ESP = GameObject.FindObjectOfType<EnemySearchPattern>();
        m_SaveSpeed = m_ESP.m_Speed;
    }

    void Update()
    {
        switch (m_TypeEnemy)
        {
            case TypeEnemy.Soldier:
                Soldier();
                break;
            case TypeEnemy.Archer:
                Archer();
                break;
            case TypeEnemy.Tank:
                Tank();
                break;
        }

        if(Physics.Raycast(transform.position, new Vector3(transform.position.x, transform.position.y, transform.position.z), out m_Hit, 5f))
        {
            if(this.name != m_Hit.transform.gameObject.name)
            {
                Debug.Log("Enemy Gevonden!");
            }            
        }
    }

    private void CheckTeam()
    {
        //----------------------------------------------------------------------------------------------------------------------------------------
        //Friendly Check
        //----------------------------------------------------------------------------------------------------------------------------------------

        RaycastHit2D _Hit = Physics2D.Raycast(transform.position, transform.TransformDirection(Vector3.right), m_RayRangeFriendly, m_Friendly);

        if (_Hit.collider != null && _Hit.collider.gameObject != this.gameObject)
        {
            m_ESP.m_Speed = 0;
            Debug.Log("Friendly");
        }
        else
        {
            m_ESP.m_Speed = m_SaveSpeed;
        }

        //----------------------------------------------------------------------------------------------------------------------------------------
        //Ai Check
        //----------------------------------------------------------------------------------------------------------------------------------------

        RaycastHit2D _Hits = Physics2D.Raycast(transform.position, transform.TransformDirection(Vector3.right), m_RayRangeAI, m_AI);

        if (_Hits.collider != null && _Hits.collider.gameObject != this.gameObject)
        {
            m_ESP.m_Speed = 0;
            Debug.Log("Enemy");
        }
        else
        {
            m_ESP.m_Speed = m_SaveSpeed;
        }
    }

    private void Soldier()
    {
        CheckTeam();
    }
    private void Archer()
    {
        CheckTeam();
    }
    private void Tank()
    {
        CheckTeam();
    }
}
